﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTrigger : MonoBehaviour
{
    public Transform spot1;
    public Transform spot2;
    public Transform spot3;
    public Transform spot4;
    public int counter;

    void Start()
    {
        counter = 0;
    }
    void OnTriggerEnter(Collider collider) 
    {
        if (collider.gameObject.tag == "Boxes")
        {
            counter++;
          //  Debug.Log("this is  " + counter);
            // gameObject.transform.position = spot1;
          if (counter == 2)
            {
                collider.gameObject.transform.position = spot1.transform.position;
                counter++;
                //Debug.Log("this is +1 " + counter);
            }
            if (counter == 5)
            {
                collider.gameObject.transform.position = spot2.transform.position;
                counter++;
               // Debug.Log("This is +2 " + counter);
            }
            if (counter == 8)
            {
                collider.gameObject.transform.position = spot3.transform.position;
                counter++;
                //Debug.Log(counter);
            }

            if (counter == 11)
            {
                collider.gameObject.transform.position = spot4.transform.position;
                counter=0;
            }

        }
        
    }

    private IEnumerator TeleportDelay(Collider collider)
    {
        yield return new WaitForSeconds(5.0f);
        
    }
}
