﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using ACQ.QuestSystem;


public class DevManager : MonoBehaviour
{
    public Button button;
    public Image image;
    public GameObject model;
    public GameObject game;
    public NavMeshSurface surface;
    public Image screen;

    // Start is called before the first frame update
    void Start()
    {
        QuestManager.Instance.CompleteStep();
        button.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (QuestManager.Instance.CurrentStep == 6)
        {
            button.gameObject.SetActive(false);
            model.gameObject.SetActive(false);
            game.gameObject.SetActive(false);
            screen.gameObject.SetActive(false);

        }
    }

    public void TaskOnClick()
    {
        QuestManager.Instance.CompleteStep();
        //button.gameObject.SetActive(false);
        if(QuestManager.Instance.CurrentStep == 2)
        {
            image.gameObject.SetActive(true);
        }

        if (QuestManager.Instance.CurrentStep == 3)
        {
            image.gameObject.SetActive(false);
            model.gameObject.SetActive(true);
        }

        if (QuestManager.Instance.CurrentStep == 4)
        {
            button.gameObject.SetActive(false);
            model.gameObject.SetActive(false);
            game.gameObject.SetActive(true);
            surface.BuildNavMesh();
        }
        if (QuestManager.Instance.CurrentStep == 5)
        {
            button.gameObject.SetActive(false);
            model.gameObject.SetActive(false);
            game.gameObject.SetActive(false);
            screen.gameObject.SetActive(false);
            QuestManager.Instance.CompleteStep();
           
        }

        if (QuestManager.Instance.CurrentStep == 6)
        {
            button.gameObject.SetActive(false);
            model.gameObject.SetActive(false);
            game.gameObject.SetActive(false);
            screen.gameObject.SetActive(false);

        }
        Debug.Log("You have clicked the button!");
    }
}
