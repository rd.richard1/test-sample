﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIMove : MonoBehaviour
{
    [SerializeField]
    Transform _destination;
    
    [SerializeField]
    Transform AI;
    public Transform Carry1;
    [SerializeField]
    Transform Carry2;
    [SerializeField]
    Transform Carry3;
    [SerializeField]
    Transform Carry4;

    [SerializeField]
    GameObject textPoints;


    [SerializeField]
    int c;
    public static int Coins;
    [SerializeField]
    Transform[] points;
    private int destinationPoint = 0;

    public List<GameObject> targets;
    [SerializeField]
    private float remainingDistance = 0.5f;

    NavMeshAgent _navMeshAgent;
    int counter;
    List<GameObject> boxList;

    // Start is called before the first frame update
    void Start()
    {
        Coins = 0;
        targets = new List<GameObject>();
        counter = 0;
        boxList = null;
        _navMeshAgent = this.GetComponent<NavMeshAgent>();

        if(_navMeshAgent == null)
        {
            Debug.LogError("this nav mesh agent component is not attached to " + gameObject.name);
        }
        else
        {
            GoToNextPoint();
        }
    }

    void GoToNextPoint()
    {
        if(points.Length == 0)
        {
            Debug.LogError("You must setup atleast one destination point");
            return;
        }

        _navMeshAgent.destination = points[destinationPoint].position;
        destinationPoint = (destinationPoint + 1) % points.Length;
    }

    private void Update()
    {
        if(!_navMeshAgent.pathPending  && _navMeshAgent.remainingDistance < remainingDistance)
        {
            GoToNextPoint();
        }
        c = Coins;
       // Debug.Log(Coins);
    }
   

    // Update is called once per frame
    void SetDestination()
    {
        if(_destination != null)
        {
            Vector3 targetVector = _destination.transform.position;
            _navMeshAgent.SetDestination(targetVector);
        }
    }
}
