﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScore : MonoBehaviour
{
    void Start()
    {
        //coins = 0;
    }
    void OnTriggerExit(Collider collider)
    {
        
        if (collider.gameObject.tag == "AI")
        {
           GameManager.coins += AIMove.Coins;
           AIMove.Coins = 0;
            
            Debug.Log("GameManager COINS: " + GameManager.coins + "AI COINS: " + AIMove.Coins);
        }
    }
}
