﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyorbelt_02 : MonoBehaviour
{
    public GameObject belt;
    public Transform endpoint;
    public float currentSpeed;
    
    public GameObject powerSwitch;
   
    public GameObject buttonHit;
    GameObject mainCamera;
    public static bool beltOn;

    void Start()
    {
        beltOn = true;
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        powerSwitch.GetComponent<Renderer>().material.color = Color.red;
        buttonHit = null;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Debug.Log("Pressed primary button.");
            if(CheckForButton())
            {
                if(buttonHit == powerSwitch)
                {
                    Debug.Log("power button.");
                    PowerSwitch();
                }
                
            }

        }
    }

    void PowerSwitch()
    {
        if(beltOn)
        {
            beltOn = false;
            powerSwitch.GetComponent<Renderer>().material.color = Color.green;
        }
        else 
        {
            beltOn = true;
            powerSwitch.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    bool CheckForButton()
    {
        bool rayHitButton = false;

        Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit)) 
        {
            if(hit.collider.gameObject == powerSwitch)
            {
                rayHitButton = true;
                buttonHit = powerSwitch;
            }
        }

        return rayHitButton;
    }

    void OnTriggerStay(Collider other)
    {
        if(beltOn)
        {
            other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint.position, currentSpeed * Time.deltaTime);

        }
    }
 
}
