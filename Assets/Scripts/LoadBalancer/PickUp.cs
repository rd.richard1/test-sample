﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [SerializeField]
    GameObject textPoints;
    [SerializeField]
    int pointAmount;
    GameObject mainCamera;
    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        //coins = 0;
    }
    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == "AI")
        {

            AIMove.Coins += pointAmount;
            ShowPoints(collider);
            Destroy(gameObject);
        }
        else
        {
        }
    }

    void ShowPoints(Collider collider)
    {
        var p = Instantiate(textPoints, collider.gameObject.transform.position, mainCamera.GetComponent<Camera>().transform.rotation, collider.gameObject.transform);
      p.GetComponent<TextMeshPro>().text = pointAmount.ToString();
    }
}
