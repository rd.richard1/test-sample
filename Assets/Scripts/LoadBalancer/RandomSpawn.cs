﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public GameObject spawnObj;
    public float SetTimer = 1;

    public float timer;

    public Transform[] spawnLocations;

    void Start()
    {
        timer = SetTimer;
    }
    void Update()
    {
        timer -= Time.deltaTime;

        if(timer <= 0f)
        {
            SpawnObject();
            timer = SetTimer;
        }
        
    }
    void SpawnObject()
    {

        int spawn = Random.Range(0, 4);
        switch (spawn)
        {
            case 0:
                if(Conveyorbelt.beltOn)
                {
                    Instantiate(spawnObj, spawnLocations[0].transform.position, Quaternion.identity);
                }
                break;
            case 1:
                if (Conveyorbelt_01.beltOn)
                {
                    Instantiate(spawnObj, spawnLocations[1].transform.position, Quaternion.identity);
                }
                break;
            case 2: 
                if(Conveyorbelt_02.beltOn)
                {
                    Instantiate(spawnObj, spawnLocations[2].transform.position, Quaternion.identity);
                }
                break;
            case 3:
                if(Conveyorbelt_03.beltOn)
                {
                    Instantiate(spawnObj, spawnLocations[3].transform.position, Quaternion.identity);
                }
                break;

        }
        Debug.Log("Location spawn: " + spawn);

    //   Instantiate(spawnObj, new Vector3(-314.3f, 2.3f, 234.5f), Quaternion.identity);
      //  Instantiate(spawnObj, new Vector3(x, y, z), Quaternion.identity) as GameObject;
     
    }
}
