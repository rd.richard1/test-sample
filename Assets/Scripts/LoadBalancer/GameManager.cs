﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int Coins = coins;
    public static int coins;
    public TextMeshProUGUI scoreKeeper;
    public Image screen;

    // Start is called before the first frame update
    void Start()
    {
        coins = 0;
        scoreKeeper.GetComponent<TextMeshProUGUI>().text = coins.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        scoreKeeper.GetComponent<TextMeshProUGUI>().text = coins.ToString();
        if(coins >= 100)
        {
            scoreKeeper.GetComponent<TextMeshProUGUI>().text = "";
            screen.gameObject.SetActive(true);
            // show screen 
            // show button 
        }
        //Debug.Log("Total Score: "+coins);
        
    }
}
