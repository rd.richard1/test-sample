﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ACQ.QuestSystem;
using ACQ.SceneManagement;

namespace ACQ
{
    namespace Plant
    {
        namespace PlantGameManagement
        {
            /// <summary>
            /// The <c>PlantGameManager</c> manages the plant minigame.
            /// </summary>
            public class PlantGameManager : MonoBehaviour
            {
                [SerializeField] LevelManager m_levelmanager;
                [SerializeField] PlantSwitcher m_switcher;

                int m_gamestep;

                //When finding the QR it completes a step.
                void Start()
                {
                    QuestManager.Instance.CompleteStep();
                }

                /// <summary>
                /// Completes a step towards the quest.
                /// </summary>
                public void CompleteGamestep()
                {
                    m_gamestep++;
                    QuestManager.Instance.CompleteStep();
                }

                /// <summary>
                /// Calls the funtion from the <c>PlantSwitcher</c> to remove objects from the ONlist.
                /// </summary>
                /// <param name="_obj">The object you want to remove.</param>
                public void RemoveObjectInONList(GameObject _obj)
                {
                    m_switcher.RemoveMeFromONList(_obj);
                }

                /// <summary>
                /// Calls the funtion from the <c>PlantSwitcher</c> to remove objects from the OFFlist.
                /// </summary>
                /// <param name="_obj">The object you want to remove.</param>
                public void RemoveObjectInOFFList(GameObject _obj)
                {
                    m_switcher.RemoveMeFromOFFList(_obj);
                }
            }
        }
    }
}