﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ACQ
{
    namespace Plant
    {
        namespace PlantGameManagement
        {
            /// <summary>
            /// <c>PlantSwitcher</c> hold everything related to switching to the cutout version of the plant.
            /// </summary>
            public class PlantSwitcher : MonoBehaviour
            {
                [SerializeField] PlantGameManager m_plantgamemanager;

                [SerializeField] List<GameObject> m_turnoninview;
                [SerializeField] List<GameObject> m_turnoffinview;

                [SerializeField] Animator m_animator;
                [SerializeField] Image m_switchindicator;

                bool m_insideview;

                /// <summary>
                /// Switches view to the whole plant or the cutout version depending on what version you are currently seeing.
                /// </summary>
                public void SwitchView()
                {
                    switch (m_insideview)
                    {
                        case false:
                            m_insideview = true;
                            m_animator.SetBool("InView", m_insideview);
                            m_switchindicator.color = Color.green;
                            SwitchON();
                            break;

                        case true:
                            m_insideview = false;
                            m_animator.SetBool("InView", m_insideview);
                            m_switchindicator.color = Color.red;
                            SwitchOFF();
                            break;
                    }
                }

                /// <summary>
                /// Switches objects on when inside the plant cutout view.
                /// </summary>
                public void SwitchON()
                {
                    foreach (GameObject obj in m_turnoffinview)
                    {
                        obj.SetActive(false);
                    }
                    foreach (GameObject obj in m_turnoninview)
                    {
                        obj.SetActive(true);
                    }
                }

                /// <summary>
                /// Switches objects off when inside the plant cutout view.
                /// </summary>
                public void SwitchOFF()
                {
                    foreach (GameObject obj in m_turnoffinview)
                    {
                        obj.SetActive(true);
                    }
                    foreach (GameObject obj in m_turnoninview)
                    {
                        obj.SetActive(false);
                    }
                }

                /// <summary>
                /// Looks into to ONlist to find the object to remove.
                /// </summary>
                /// <param name="_obj">The object you want to remove.</param>
                public void RemoveMeFromONList(GameObject _obj)
                {
                    for (int o = 0; o < m_turnoninview.Count - 1; o++)
                    {
                        if (_obj == m_turnoninview[o].gameObject)
                        {
                            m_turnoninview.RemoveAt(o);
                        }
                    }
                }

                /// <summary>
                /// Looks into to OFFlist to find the object to remove.
                /// </summary>
                /// <param name="_obj">The object you want to remove.</param>
                public void RemoveMeFromOFFList(GameObject _obj)
                {
                    for (int o = 0; o < m_turnoffinview.Count - 1; o++)
                    {
                        if (_obj == m_turnoffinview[o].gameObject)
                        {
                            m_turnoffinview.RemoveAt(o);
                        }
                    }
                }
            }
        }
    }
}