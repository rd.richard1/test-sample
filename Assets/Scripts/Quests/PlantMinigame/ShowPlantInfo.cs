﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ACQ
{
    namespace Plant
    {
        namespace PlantGameManagement
        {
            /// <summary>
            /// <c>ShowPlantInfo</c> manages everything related to showing info about the plant.
            /// </summary>
            public class ShowPlantInfo : MonoBehaviour
            {
                [SerializeField] TextMeshProUGUI m_title;
                [SerializeField] TextMeshProUGUI m_description;

                bool m_inplantinfoscreen;

                /// <summary>
                /// Changes text in the info panel
                /// </summary>
                /// <param name="_title">Tilte of the info</param>
                /// <param name="_description">Info description</param>
                public void ChangeText(string _title, string _description)
                {
                    m_title.SetText(_title);
                    m_description.SetText(_description);
                }

                /// <summary>
                /// Gets or sets if you are in the plant info screen or not.
                /// </summary>
                public bool InPlantInfoScreen
                {
                    get { return m_inplantinfoscreen; }
                    set { m_inplantinfoscreen = value; }
                }
            }
        }
    }
}
