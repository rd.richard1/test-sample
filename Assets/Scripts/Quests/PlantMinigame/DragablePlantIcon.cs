﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using ACQ.DragAndDrop;
using ACQ.DialogSystem;
using ACQ.Plant.PlantGameManagement;

namespace ACQ
{
    namespace Plant
    {
        namespace DragAndDrop
        {
            /// <summary>
            /// <c>DragablePlantIcon</c> inherits from <c>DragAndDropable</c>. This class manages the drag and dropable UI icons in the plant <c>Quest</c>.
            /// </summary>
            public class DragablePlantIcon : DragAndDropable
            {
                [SerializeField] PlantGameManager m_plantgamemanager;
                [SerializeField] ShowPlantInfo m_infoshower;

                [SerializeField] Transform m_startpos;
                [SerializeField] List<GameObject> m_objectstosetactive;
                [SerializeField] GameObject m_myicon;
                [SerializeField] Camera m_camera;

                [SerializeField] string m_iconname;

                [SerializeField] string m_infotitle;

                [TextArea(3, 6)]
                [SerializeField] string m_infodescription;

                /// <summary>
                /// Gets called whenever you're dragging this gameobject. It stays at the same position as where the user touches.
                /// </summary>
                /// <param name="eventData">Data of the current pointer.</param>
                public override void OnDrag(PointerEventData eventData)
                {
                    if (DialogManager.Instance.InConversation == false && m_infoshower.InPlantInfoScreen == false)
                    {
                        transform.position = Input.GetTouch(0).position;
                    }
                }

                /// <summary>
                /// Gets called when you release the gameobject.
                /// </summary>
                /// <remarks>
                /// <para>It will cast a ray at touch location and check if the hitted object
                /// is a droppoint. If it is a droppoint it will checks what object it hit and if its the correct sensor it will show the object.
                /// Else it will just reset its position.</para>
                /// </remarks>
                /// <param name="eventData">Data of the current pointer.</param>
                public override void OnEndDrag(PointerEventData eventData)
                {
                    if (DialogManager.Instance.InConversation == false && m_infoshower.InPlantInfoScreen == false)
                    {
                        Ray ray = m_camera.ScreenPointToRay(Input.GetTouch(0).position);
                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("Droppoint"))
                        {
                            if (m_iconname == hit.collider.gameObject.GetComponent<DropPoint>().BuildToChangeInto)
                            {
                                foreach (GameObject obj in m_objectstosetactive)
                                {
                                    obj.SetActive(true);
                                }

                                m_plantgamemanager.RemoveObjectInONList(hit.collider.gameObject);
                                hit.collider.gameObject.SetActive(false);
                                m_infoshower.ChangeText(m_infotitle, m_infodescription);
                                m_plantgamemanager.CompleteGamestep();
                                m_plantgamemanager.RemoveObjectInONList(gameObject);
                                m_myicon.SetActive(false);
                            }
                            else
                            {
                                transform.position = m_startpos.position;
                            }
                        }
                        else
                        {
                            transform.position = m_startpos.position;
                        }
                    }
                }
            }
        }
    }
}
