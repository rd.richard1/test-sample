﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using ACQ.DialogSystem;

namespace ACQ
{
    namespace QuestSystem
    {
        /// <summary>
        /// The <c>QuestGiver</c> class contains everything related to giving a <c>Quest</c> through buildings that are located in the City.
        /// </summary>
        /// <remarks>
        /// <para>This script has to be put on all the buildings that will give a <c>Quest</c> to you.
        /// You will also have to give it some object to turn on after the completion of the Quest.</para>
        /// <para>The system will first look if the quest has ever been completed. If not you will get prompt where you can accept the <c>Quest</c>.</para>
        /// </remarks>
        public class QuestGiver : MonoBehaviour
        {
            [SerializeField] Quest m_questtogive;
            [SerializeField] GameObject[] m_buildingstoupgrade;

            /// <summary>
            /// Unity function that gets called when click or tap on the object.
            /// </summary>
            public void OnMouseDown()
            {
                if (m_questtogive != null)
                {
                    if (QuestManager.Instance.CompletedQuests.Count != 0)
                    {
                        bool iscompleted = false;

                        for (int q = 0; q < QuestManager.Instance.CompletedQuests.Count; q++)
                        {
                            if (m_questtogive == QuestManager.Instance.CompletedQuests[q])
                            {
                                iscompleted = true;
                                break;
                            }
                        }

                        if (!iscompleted)
                        {
                            if (!DialogManager.Instance.InConversation && !QuestManager.Instance.IsQuestPanelShowing &&
                                !QuestManager.Instance.HasQuest)
                            {
                                DialogManager.Instance.StartDialog(m_questtogive.QuestDialogs[0]);
                                AddBuildingsToUpgradeList();
                                StartCoroutine(ICheckIfDialogIsFinished());
                            }
                        }
                    }
                    else
                    {
                        if (!DialogManager.Instance.InConversation && !QuestManager.Instance.IsQuestPanelShowing &&
                            !QuestManager.Instance.HasQuest)
                        {
                            DialogManager.Instance.StartDialog(m_questtogive.QuestDialogs[0]);
                            AddBuildingsToUpgradeList();
                            StartCoroutine(ICheckIfDialogIsFinished());
                        }
                    }
                }
            }

            void AddBuildingsToUpgradeList()
            {
                m_questtogive.BuildingsToSetActive = m_buildingstoupgrade;
            }

            /// <summary>
            /// Waits for the dialog to be done. Then takes action.
            /// </summary>
            /// <returns>returns null so that it loops back when you are in a conversation</returns>
            IEnumerator ICheckIfDialogIsFinished()
            {
                while (DialogManager.Instance.InConversation)
                {
                    yield return null;
                }

                QuestManager.Instance.ShowQuestAcceptPanel(m_questtogive);
            }
        }
    }
}