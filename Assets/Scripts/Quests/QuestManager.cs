﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using ACQ.DialogSystem;
using ACQ.UpgradeSystem;

namespace ACQ
{
    namespace QuestSystem
    {
        /// <summary>
        /// The <c>QuestManager</c> class is a Singleton that manages everything Quest related.
        /// </summary>
        /// <remarks>
        /// <para>It can accept or decline the Quest, Checks if its completed, Complete a step towards the Quest and sets the assigment.</para>
        /// </remarks>
        public class QuestManager : MonoBehaviour
        {
            public static QuestManager Instance;

            [SerializeField] GameObject m_questpanel;

            [SerializeField] TextMeshProUGUI m_questtitlebox;
            [SerializeField] TextMeshProUGUI m_questdescriptionbox;
            [SerializeField] TextMeshProUGUI m_queststepassingmentbox;
            [SerializeField] GameObject m_debuginstantqueststepcompleter;

            Quest m_currentquest;

            bool m_hasquest;
            bool m_showingquestacceptionpanel;

            List<Quest> m_completedquests;

            int m_currentstep;

            void Awake()
            {
                //Instantiate Singleton pattern 
                if (Instance != null && Instance != this)
                {
                    //Destroy and return if singleton is already there
                    Destroy(this.gameObject);
                    return;
                }

                Instance = this;
                DontDestroyOnLoad(this.gameObject);

                m_completedquests = new List<Quest>();
            }

            /// <summary>Checks if the current step is the same as the steps needed to complete the <c>Quest</c>.</summary>
            /// <remarks>
            /// <para>If the the steps are under the requirement the system will give the next <c>Quest</c> assignment
            /// instead and trigger <c>Dialog</c> (if it exists) related to which step you completed.</para>
            /// </remarks>
            void CheckIfQuestIsCompleted()
            {
                if (m_currentstep == m_currentquest.StepsNeededToComplete)
                {
                    CompleteQuest();
                }
                else
                {
                    if (m_currentquest.QuestDialogs[m_currentstep] != null)
                    {
                        DialogManager.Instance.StartDialog(m_currentquest.QuestDialogs[m_currentstep]);
                    }

                    SetNewQuestAssignment();
                }
            }

            /// <summary>This will open up the Quest acceptence panel in game.</summary>
            /// <remarks>
            /// <para>This will also change the text to the Quest that you got.</para>
            /// </remarks>
            /// <param name="_quest">The <c>Quest</c> that you want to show for acceptence</param>
            public void ShowQuestAcceptPanel(Quest _quest)
            {
                SetQuest(_quest);
                m_showingquestacceptionpanel = true;
                m_questpanel.SetActive(true);
                m_questtitlebox.SetText(_quest.QuestTitle);
                m_questdescriptionbox.SetText(m_currentquest.QuestDescription);
            }

            /// <summary>This will complete 1 step towards the quest and then check if its completed.</summary>
            public void CompleteStep()
            {
                m_currentstep++;
                Debug.Log("CurrentStep: " + m_currentstep);

                CheckIfQuestIsCompleted();
            }

            /// <summary>This will accept the <c>Quest</c> that you got.</summary>
            public void AcceptQuest()
            {
                m_showingquestacceptionpanel = false;
                m_questpanel.SetActive(false);

                m_hasquest = true;
                SetNewQuestAssignment();

                if (m_currentquest.TESTMODE) { m_debuginstantqueststepcompleter.SetActive(true); }
            }

            /// <summary>This will decline the <c>Quest</c> that you got.</summary>
            public void DeclineQuest()
            {
                m_showingquestacceptionpanel = false;
                m_questpanel.SetActive(false);
            }
            /// <summary>This will set the current <c>Quest</c>.</summary>
            /// <param name="_quest">The <c>Quest</c> that you want to set.</param>
            void SetQuest(Quest _quest)
            {
                m_currentquest = _quest;
            }

            /// <summary>This will set the assignment of the current <c>Quest</c> per Quest step taken.</summary>
            void SetNewQuestAssignment()
            {
                m_queststepassingmentbox.SetText(m_currentquest.QuestAssignmentProgress[m_currentstep]);
            }

            /// <summary>This will complete the current Quest.</summary>
            /// <remarks>
            /// <para>It will also add the quest to the completed quests list. Then it will show the last dialog that
            /// is available and removes the current Quest so the program will know that you don't have a <c>Quest</c> anymore.</para>
            /// <para>This will also upgrade the City.</para>
            /// </remarks>
            void CompleteQuest()
            {
                m_completedquests.Add(m_currentquest);
                DialogManager.Instance.StartDialog(m_currentquest.QuestDialogs[m_currentstep]);
                m_hasquest = false;
                m_currentstep = 0;
                m_queststepassingmentbox.SetText("");
                StartCoroutine(BuildingUpgrader.Instance.IUpgradeBuildings(m_currentquest.BuildingsToSetActive, 0.1f));

                if (m_currentquest.TESTMODE) { m_debuginstantqueststepcompleter.SetActive(false); }
            }

            /// <summary>
            /// Returns a bool that indicates if you are in the <c>Quest</c> acceptence panel.
            /// </summary>
            public bool IsQuestPanelShowing
            {
                get { return m_showingquestacceptionpanel; }
            }

            /// <summary>
            /// Returns a bool that indicates if already have a <c>Quest</c>.
            /// </summary>
            public bool HasQuest
            {
                get { return m_hasquest; }
            }

            /// <summary>
            /// Returns a int that indicates at which step you are in the <c>Quest</c>. 
            /// </summary>
            public int CurrentStep
            {
                get { return m_currentstep; }
            }

            /// <summary>
            /// Returns a list with completed <c>Quests</c>.
            /// </summary>
            public List<Quest> CompletedQuests
            {
                get { return m_completedquests; }
            }

            /// <summary>
            /// Returns the current <c>Quest</c>
            /// </summary>
            public Quest CurrentQuest
            {
                get { return m_currentquest; }
            }
        }
    }
}