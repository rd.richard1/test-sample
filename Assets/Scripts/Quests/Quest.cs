﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using ACQ.DialogSystem;

namespace ACQ
{
    namespace QuestSystem
    {
        /// <summary>
        /// The <c>Quest</c> class is a ScriptableObject class that holds all the data related to the Quest.
        /// </summary>
        /// <remarks>
        /// <para>Each <c>Quest</c> needs a number of steps that it has to take to be completed. Because finding the QR will always be one step </para>
        /// </remarks>
        [CreateAssetMenu(fileName = "QuestData", menuName = "Create Scriptable/New Quest", order = 1)]
        public class Quest : ScriptableObject
        {
            [Tooltip("Title of the Quest. This will be displayed when you get a Quest in the game.")]
            public string QuestTitle;

            [TextArea(3, 6), Tooltip("Description of the Quest. This will be displayed when you get a Quest in the game.")]
            public string QuestDescription;

            [Tooltip("Assignments you need to complete for the Quest. Every step will be its own assignment and is displayed up top. So 5 steps are 4 assignments")]
            public string[] QuestAssignmentProgress;

            [Tooltip("This number always needs to be 1 more then the assignments. This is how many steps you will need to take to complete the enitre Quest.")]
            public int StepsNeededToComplete;

            [Tooltip("This is the Dialog it will say everytime a step is completed. When you have a step that doesn't need a Dialog, then just leave it null")]
            public Dialog[] QuestDialogs;

            [Tooltip("These are the objects that will be set on active upon completion of the Quest. Use it to turn on objects in the City to simulate an upgrade effect.")]
            public GameObject[] BuildingsToSetActive;

            [Tooltip("The QR code that is related to this quest.")]
            public string QRCode;

            [Tooltip("This will make a big complete button where you can instantly complete the Quest. DESIGNED FOR DEBUGGING")]
            public bool TESTMODE;
        }
    }
}