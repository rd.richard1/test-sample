﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using ACQ.QuestSystem;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace ChainManagement
        {
            /// <summary>
            /// The <c>ChainManager</c> manages everything related to the chain process.
            /// </summary>
            public class ChainManager : MonoBehaviour
            {
                [SerializeField] GameObject m_chaininfoprefab;

                List<GameObject> m_chainlist;
                [SerializeField] GameObject m_chaininfocontentpanel;

                [SerializeField] GameObject m_chainmessageincidactor;
                [SerializeField] TextMeshProUGUI m_chainmessagenumber;
                [SerializeField] Scrollbar m_contentscrollbar;

                int m_newchainmessage;
                bool m_inchainview;

                bool m_chainstepcomplete;

                void Awake()
                {
                    m_chainlist = new List<GameObject>();
                }

                void Update()
                {
                    if (m_inchainview)
                    {
                        ReadedNewMessages();
                    }
                }

                /// <summary>
                /// Instantiates the ChainInfo prefab that holds text and replaces that text with
                /// information about the current chain that happened.
                /// </summary>
                /// <param name="chaininfo">The current chain that happened.</param>
                public void AddChain(string chaininfo)
                {
                    GameObject obj = Instantiate(m_chaininfoprefab, m_chaininfocontentpanel.transform);
                    obj.GetComponent<ChainInfo>().SetChainInfo(chaininfo);
                    m_chainlist.Add(obj);
                    m_newchainmessage++;
                    RearrangeChainlistView();

                    //When this is called, it will complete 1 step towards the current Quest.
                    if (!m_chainstepcomplete)
                    {
                        QuestManager.Instance.CompleteStep();
                        m_chainstepcomplete = true;
                    }
                }

                /// <summary>
                /// When a chain has been made, it is put into the right position here.
                /// </summary>
                /// <remarks>
                /// <para>This will also tell the message indicator that a new Chain has been added</para>
                /// </remarks>
                void RearrangeChainlistView()
                {
                    m_contentscrollbar.value = 1;

                    for (int chain = m_chainlist.Count - 1; chain >= 0; chain--)
                    {
                        m_chainlist[chain].GetComponent<RectTransform>().position = new Vector3(725, 1850 - (225 * chain), 0);
                    }

                    m_chainmessageincidactor.SetActive(true);
                    m_chainmessagenumber.SetText(m_newchainmessage.ToString());
                }

                /// <summary>
                /// When a chain has been made, it is put into the right position here.
                /// </summary>
                /// <remarks>
                /// <para>This will also tell the message indicator that a new Chain has been added</para>
                /// </remarks>
                public void ReadedNewMessages()
                {
                    m_chainmessageincidactor.SetActive(false);
                    m_newchainmessage = 0;
                }

                /// <summary>
                /// Gets and sets if you are currently inside the Chainpanel or not.
                /// </summary>
                public bool InChainView
                {
                    get { return m_inchainview; }
                    set { m_inchainview = value; }
                }
            }
        }
    }
}