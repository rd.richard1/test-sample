﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACQ
{
    namespace DragAndDrop
    {
        /// <summary>
        /// The <c>DropPoint</c> class is a point where drag and dropables can drop on.
        /// </summary>
        public class DropPoint : MonoBehaviour
        {
            [SerializeField] string m_changeinto;

            /// <summary>
            /// Gets the building to change into.
            /// </summary>
            public string BuildToChangeInto
            {
                get { return m_changeinto; }
            }
        }
    }
}
