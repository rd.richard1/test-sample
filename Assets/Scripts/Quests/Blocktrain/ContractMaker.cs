﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using ACQ.QuestSystem;
using ACQ.Blocktrain.Buildings;
using ACQ.Blocktrain.Train;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace ChainManagement
        {
            /// <summary>
            /// The <c>ContractMaker</c> takes care of of the whole contract process.
            /// </summary>
            /// <remarks>
            /// <para>This class can setup contracts, send contracts towards buildings and open
            /// panels where you can fill in the contract.</para>
            /// </remarks>
            public class ContractMaker : MonoBehaviour
            {
                [SerializeField] ChainManager m_chainmanager;
                [SerializeField] TrainManager m_trainmanger;

                [SerializeField] GameObject m_contractsetuppanel;
                [SerializeField] TextMeshProUGUI[] m_companiestext;

                [SerializeField] GameObject[] m_objectstoturnoffwhilecontractmaking;
                [SerializeField] GameObject[] m_contractbuttons;

                [SerializeField] GameObject m_realcontract;
                [SerializeField] TextMeshProUGUI m_legablecontract;
                [SerializeField] TextMeshProUGUI m_goodsacceptence;

                List<SelectableBuilding> m_contractbuildings;

                Camera m_camera;

                bool m_buyeraccepted;
                bool m_selleraccepted;
                bool m_cancreatecontract;

                bool m_contractmakestepcompletion;
                bool m_chaingamebegin;

                void Start()
                {
                    m_contractbuildings = new List<SelectableBuilding>();
                    m_camera = FindObjectOfType<Camera>();
                }


                // When this object is turned on it will complete a step towards the quest
                void OnEnable()
                {
                    if (QuestManager.Instance.HasQuest)
                    {
                        if (m_chaingamebegin != true)
                        {
                            QuestManager.Instance.CompleteStep();
                            m_chaingamebegin = true;
                        }
                    }
                }

                /// <summary>
                /// This function will check if the screen has been touched or not.
                /// </summary>
                /// <returns>A boolean which indicates if the screen has been touched or not.</returns>
                bool RecievedTouchedDown()
                {
                    if (Input.touches.Length > 0)
                    {
                        if (Input.GetTouch(0).phase == TouchPhase.Began)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                void Update()
                {
                    //This is the building selection when you have are creating a contract.
                    //Each time you tap a ray gets cast at your fingers location and it will check if its a SelectableBuilding.
                    //If this object has the right tag it will add its data to the list (this data will be compared later).
                    //You have to select two buildings to make a contract.
                    if (m_contractbuildings.Count < 2)
                    {
                        if (m_cancreatecontract == true && RecievedTouchedDown())
                        {
                            Ray ray = m_camera.ScreenPointToRay(Input.GetTouch(0).position);
                            RaycastHit hit;

                            if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("SelectableBlocktrainBuilding"))
                            {
                                m_contractbuildings.Add(hit.collider.gameObject.GetComponent<SelectableBuilding>());

                                for (int b = 0; b < m_contractbuildings.Count; b++)
                                {
                                    m_companiestext[b].SetText(m_contractbuildings[b].BuildingData.BuildingName);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < m_contractbuttons.Length; i++)
                        {
                            m_contractbuttons[i].SetActive(true);
                        }
                    }
                }

                /// <summary>
                /// Sets up the ability to setup a contract between two buildings.
                /// </summary>
                /// <remarks>
                /// <para>Will activate the contract making panel as well</para>
                /// </remarks>
                public void InitiateContractMaking()
                {
                    m_cancreatecontract = true;
                    m_contractsetuppanel.SetActive(true);

                    if (m_contractbuildings.Count > 0)
                    {
                        m_contractbuildings.Clear();
                    }

                    for (int i = 0; i < m_contractbuttons.Length; i++)
                    {
                        m_contractbuttons[i].SetActive(false);
                    }

                    for (int t = 0; t < m_companiestext.Length; t++)
                    {
                        m_companiestext[t].SetText("[select a building]");
                    }

                    for (int obj = 0; obj < m_objectstoturnoffwhilecontractmaking.Length; obj++)
                    {
                        m_objectstoturnoffwhilecontractmaking[obj].SetActive(false);
                    }
                }

                /// <summary>
                /// Clears the current contract containments for reusability
                /// </summary>
                public void ResetContractMaking()
                {
                    for (int t = 0; t < m_companiestext.Length; t++)
                    {
                        m_companiestext[t].SetText("[select a building]");
                    }

                    for (int i = 0; i < m_contractbuttons.Length; i++)
                    {
                        m_contractbuttons[i].SetActive(false);
                    }

                    m_contractbuildings.Clear();
                }

                /// <summary>
                /// Closes the contract panel and sets other objects on active.
                /// </summary>
                public void CloseContractMaking()
                {
                    m_contractsetuppanel.SetActive(false);
                    m_realcontract.SetActive(false);

                    for (int obj = 0; obj < m_objectstoturnoffwhilecontractmaking.Length; obj++)
                    {
                        m_objectstoturnoffwhilecontractmaking[obj].SetActive(true);
                    }

                    CreatingContract = false;
                }

                /// <summary>
                /// Sends the contract to both selected buildings and starts checking if they agree.
                /// </summary>
                /// <remarks>
                /// <para>Completes a step towards the current Quest.</para>
                /// <para>Also closes the contract panel after the contracts have been sent.</para>
                /// </remarks>
                public void CreateContract()
                {
                    m_chainmanager.AddChain("A contract has been send to " + m_contractbuildings[0].BuildingData.BuildingName);
                    m_chainmanager.AddChain("A contract has been send to " + m_contractbuildings[1].BuildingData.BuildingName);
                    StartCoroutine(m_contractbuildings[0].CheckSellersContract(m_contractbuildings[1].BuildingData.ProductDemandName, m_contractbuildings[1].BuildingData.AmountToProduce));
                    StartCoroutine(m_contractbuildings[1].CheckBuyersContract(m_contractbuildings[0].BuildingData.ProductBToProduce));

                    //QUEST STEP COMPLETE
                    if (!m_contractmakestepcompletion)
                    {
                        QuestManager.Instance.CompleteStep();
                        Debug.Log("Contract step completed");
                        m_contractmakestepcompletion = true;
                    }

                    CloseContractMaking();
                }

                /// <summary>
                /// Opens the actual contract screen where you can see what is going to be delivered.
                /// </summary>
                public void OpenRealContract()
                {
                    m_realcontract.SetActive(true);
                    m_contractsetuppanel.SetActive(false);

                    m_legablecontract.SetText("This contract is a legable contract between " + m_contractbuildings[0].BuildingData.BuildingName + " " +
                        "and " + m_contractbuildings[1].BuildingData.BuildingName);

                    m_goodsacceptence.SetText(m_contractbuildings[0].BuildingData.BuildingName + " will deliver "
                        + m_contractbuildings[1].BuildingData.AmountToProduce + " " + m_contractbuildings[0].BuildingData.ProductBToProduce
                        + " to " + m_contractbuildings[1].BuildingData.BuildingName);
                }

                /// <summary>
                /// Sets if the seller has accepted the contract.
                /// </summary>
                /// <param name="acceptence">Acceptence of the contract</param>
                public void SellerAccept(bool acceptence)
                {
                    m_selleraccepted = acceptence;
                }

                /// <summary>
                /// Sets if the buyer has accepted the contract.
                /// </summary>
                /// <remarks>
                /// <para>Also calls a check if both parties agreed.</para>
                /// </remarks>
                /// <param name="acceptence">Acceptence of the contract</param>
                public void BuyerAccept(bool acceptence)
                {
                    m_buyeraccepted = acceptence;

                    CheckContractAcceptence();
                }

                /// <summary>
                /// Checks if both selected building agreed upon the contract.
                /// </summary>
                /// <remarks>
                /// <para>If the train doesn't already have an route and both parties accepted, it will create a
                /// train route between the two stations of the parties.</para>
                /// <para>If the train is in transit or one of the parties didn't accept, then it will add a chain
                /// which will indicate what happened.</para>
                /// </remarks>
                void CheckContractAcceptence()
                {
                    if (!m_trainmanger.InTransit)
                    {
                        if (m_selleraccepted == true && m_buyeraccepted == true)
                        {
                            m_trainmanger.SetSellerAndBuyer(m_contractbuildings[0], m_contractbuildings[1]);
                            StartCoroutine(m_trainmanger.DoTrainRoute());
                        }
                        else
                        {
                            m_chainmanager.AddChain("All contracts have been returned to you. Both parties must agree before a train route can be created.");
                        }
                    }
                    else
                    {
                        m_chainmanager.AddChain("Both parties accepted, but the train is still in transit. Try again later!");
                    }
                }

                /// <summary>
                /// Gets or sets if you are currently creating a contract.
                /// </summary>
                public bool CreatingContract
                {
                    get { return m_cancreatecontract; }
                    set { m_cancreatecontract = value; }
                }
            }
        }
    }
}