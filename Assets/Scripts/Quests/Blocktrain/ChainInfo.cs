﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace ChainManagement
        {
            /// <summary>
            /// The <c>ChainInfo</c> class is there to add text when a chain is created.
            /// </summary>
            public class ChainInfo : MonoBehaviour
            {
                [SerializeField] TextMeshProUGUI m_chaintext;

                /// <summary>
                /// Sets the text of the chainbox to what happened.
                /// </summary>
                /// <param name="chaininfo">The info you want to show.</param>
                public void SetChainInfo(string chaininfo)
                {
                    m_chaintext.SetText(chaininfo);
                }

                /// <summary>
                /// Returns a string that contains all the info inside the chain textbox.
                /// </summary>
                public string Info { get { return m_chaintext.text; } }
            }
        }
    }
}