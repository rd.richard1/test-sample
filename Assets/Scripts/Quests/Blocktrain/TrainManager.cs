﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ACQ.Blocktrain.Buildings;
using ACQ.Blocktrain.ChainManagement;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace Train
        {
            /// <summary>
            /// The <c>TrainManager</c> manages everything related to the movement of the train.
            /// </summary>
            public class TrainManager : MonoBehaviour
            {
                SelectableBuilding m_seller;
                SelectableBuilding m_buyer;

                [SerializeField] ChainManager m_chainmanger;

                bool m_intransit;

                /// <summary>
                /// Picks up the goods from station 1 and delivers them to station 2
                /// </summary>
                /// <remarks>
                /// <para>Wait a bit between each step to simulate and actual train process</para>
                /// </remarks>
                public IEnumerator DoTrainRoute()
                {
                    m_intransit = true;
                    m_seller.PickUp();
                    yield return new WaitForSeconds(3f);
                    m_chainmanger.AddChain("Train picked up " + m_buyer.BuildingData.ProductDemandName + " at " + m_seller.BuildingData.BuildingName);
                    yield return new WaitForSeconds(2f);
                    m_chainmanger.AddChain("Train left the " + m_seller.Station.name + " and is now on its way to " + m_buyer.Station.name);

                    while (Vector3.Distance(transform.position, m_buyer.Station.transform.position) > 0.01f)
                    {
                        float step = 0.5f * Time.deltaTime;
                        transform.LookAt(m_buyer.Station.transform);
                        transform.position = Vector3.MoveTowards(transform.position, m_buyer.Station.transform.position, step);
                        Debug.Log("Moving");
                        yield return null;
                    }

                    m_chainmanger.AddChain("Train has arrived at " + m_buyer.Station.name);
                    yield return new WaitForSeconds(3f);
                    m_buyer.Deliver();
                    yield return new WaitForSeconds(2f);
                    m_chainmanger.AddChain("The train is now empty and can be used again.");
                    m_intransit = false;
                }

                /// <summary>
                /// Sets the train route from seller to buyer.
                /// </summary>
                /// <param name="seller">The seller of the goods</param>
                /// <param name="buyer">The buyer of the goods</param>
                public void SetSellerAndBuyer(SelectableBuilding seller, SelectableBuilding buyer)
                {
                    m_seller = seller;
                    m_buyer = buyer;
                }

                /// <summary>
                /// Gets if the train is in transit or not.
                /// </summary>
                public bool InTransit
                {
                    get { return m_intransit; }
                }
            }
        }
    }
}
