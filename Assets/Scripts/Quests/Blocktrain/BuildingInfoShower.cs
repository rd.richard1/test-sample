﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using ACQ.DialogSystem;
using ACQ.Blocktrain.ChainManagement;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace Buildings
        {
            /// <summary>
            /// The <c>BuildingInfoShower</c> will manage everthing related to showing info of the building you selected.
            /// </summary>
            public class BuildingInfoShower : MonoBehaviour
            {
                [SerializeField] GameObject m_buildinginfopanel;
                [SerializeField] GameObject m_contractmakebutton;
                [SerializeField] GameObject m_chainviewbutton;

                [SerializeField] TextMeshProUGUI m_buildingtitle;
                [SerializeField] TextMeshProUGUI m_productiontext;
                [SerializeField] TextMeshProUGUI m_currentlyproducedtext;
                [SerializeField] TextMeshProUGUI m_requirementstext;

                [SerializeField] ContractMaker m_contractmaker;
                [SerializeField] ChainManager m_chainmanager;
                Camera m_camera;

                bool m_foundqr;

                void Start()
                {
                    m_camera = FindObjectOfType<Camera>();
                }

                void Update()
                {
                    //This will check if you are touching the screen and will then raycast from your fingers position.
                    //If it will hit a building, it will show info related to the building that you just hit.
                    if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary && !DialogManager.Instance.InConversation && !m_chainmanager.InChainView && !m_contractmaker.CreatingContract)
                    {
                        Ray ray = m_camera.ScreenPointToRay(Input.GetTouch(0).position);
                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("SelectableBlocktrainBuilding"))
                        {
                            Debug.Log("Hitted building!");
                            OpenBuildingInfo(hit.collider.gameObject);
                        }
                    }
                    else
                    {
                        CloseBuildingInfo();
                    }
                }

                /// <summary>
                /// Opens the info panel and shows information based upon which building is selected.
                /// </summary>
                /// <param name="_obj">The building whose info will be shown.</param>
                public void OpenBuildingInfo(GameObject _obj)
                {
                    SelectableBuilding building = _obj.GetComponent<SelectableBuilding>();

                    m_buildinginfopanel.SetActive(true);
                    m_contractmakebutton.SetActive(false);
                    m_chainviewbutton.SetActive(false);

                    m_buildingtitle.SetText(building.BuildingData.BuildingName);
                    m_productiontext.SetText("Produces: " + building.BuildingData.ProductBToProduce);
                    m_currentlyproducedtext.SetText("Current " + building.BuildingData.ProductBToProduce + " in storage: " + building.CurrentlyProducedGoods.ToString());
                    m_requirementstext.SetText("Requires: " + building.BuildingData.ProductDemandName);
                }

                /// <summary>
                /// Closes the building info panel.
                /// </summary>
                public void CloseBuildingInfo()
                {
                    m_buildinginfopanel.SetActive(false);
                    m_chainviewbutton.SetActive(true);
                    m_contractmakebutton.SetActive(true);
                }
            }
        }
    }
}