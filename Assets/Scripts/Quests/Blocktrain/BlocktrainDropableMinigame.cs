﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ACQ.DialogSystem;
using ACQ.Blocktrain.DragAndDrop;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace Minigame
        {
            /// <summary>The <c>BlocktrainDropableMinigame</c> manages the first part of the Blocktrain,
            /// which is a drag and drop minigame.</summary>
            public class BlocktrainDropableMinigame : MonoBehaviour
            {
                [SerializeField] List<GameObject> m_dropableplatforms;
                [SerializeField] List<GameObject> m_uielements;
                [SerializeField] List<GameObject> m_buildings;
                [SerializeField] List<GameObject> m_setactiveoncompletion;
                [SerializeField] List<Dialog> m_dialogs;

                [SerializeField] Dialog m_wrongplacementdialog;

                int m_gamestep;

                DragableBlocktrainUI m_blocktrainicon;

                /// <summary>
                /// Sets the current Icon to the Icon that you are dragging.
                /// </summary>
                /// <param name="_icon">The icon that is being dragged</param>
                public void SetHoldingIcon(DragableBlocktrainUI _icon)
                {
                    m_blocktrainicon = _icon;
                    Debug.Log(m_blocktrainicon.gameObject.name);
                }

                /// <summary>
                /// When you drop an icon it checks if the icon has the same plot.
                /// </summary>
                /// <param name="_buildingicon">The name of the current icon that the user is dragging.</param>
                /// <param name="_buildingplot">The name of the plot the icon has been dropped on.</param>
                public void CheckOnRelease(string _buildingicon, string _buildingplot)
                {
                    //Pls... look for a better solution then I did here.
                    switch (m_gamestep)
                    {
                        case 0:
                            if (_buildingicon == "Mine" && _buildingplot == "Mine")
                            {
                                CompleteGamestep();
                            }
                            else
                            {
                                m_blocktrainicon.ResetPosition();
                                DialogManager.Instance.StartDialog(m_wrongplacementdialog);
                            }
                            break;

                        case 1:
                            if (_buildingicon == "Aluminium" && _buildingplot == "Aluminium")
                            {
                                CompleteGamestep();
                            }
                            else
                            {
                                m_blocktrainicon.ResetPosition();
                                DialogManager.Instance.StartDialog(m_wrongplacementdialog);
                            }
                            break;

                        case 2:
                            if (_buildingicon == "Soda" && _buildingplot == "Soda")
                            {
                                CompleteGamestep();
                            }
                            else
                            {
                                m_blocktrainicon.ResetPosition();
                                DialogManager.Instance.StartDialog(m_wrongplacementdialog);
                            }
                            break;

                        case 3:
                            if (_buildingicon == "Mall" && _buildingplot == "Mall")
                            {
                                CompleteGamestep();
                            }
                            else
                            {
                                m_blocktrainicon.ResetPosition();
                                DialogManager.Instance.StartDialog(m_wrongplacementdialog);
                            }
                            break;

                        case 4:
                            if (_buildingicon == "Customer" && _buildingplot == "Customer")
                            {
                                CompleteGamestep();
                            }
                            else
                            {
                                m_blocktrainicon.ResetPosition();
                                DialogManager.Instance.StartDialog(m_wrongplacementdialog);
                            }
                            break;
                    }

                    //If you have dropped every building, then it continues with main blocktrain game.
                    if (m_gamestep == 5)
                    {
                        ChainSetupComplete();
                    }
                }

                /// <summary>
                /// Completes a step towards the completion of the minigame.
                /// </summary>
                /// <remarks>
                /// <para>This will also turn off the platform and icon insode the game. It will also start some dialog feedback.</para>
                /// </remarks>
                void CompleteGamestep()
                {
                    m_dropableplatforms[m_gamestep].SetActive(false);
                    m_uielements[m_gamestep].SetActive(false);
                    m_buildings[m_gamestep].SetActive(true);
                    DialogManager.Instance.StartDialog(m_dialogs[m_gamestep]);
                    m_gamestep++;
                }

                /// <summary>
                /// Sets the main blocktrain game on active.
                /// </summary>
                void ChainSetupComplete()
                {
                    for (int i = 0; i < m_setactiveoncompletion.Count - 1; i++)
                    {
                        m_setactiveoncompletion[i].SetActive(true);
                    }

                    DialogManager.Instance.StartDialog(m_dialogs[m_gamestep - 1]);
                }
            }
        }
    }
}