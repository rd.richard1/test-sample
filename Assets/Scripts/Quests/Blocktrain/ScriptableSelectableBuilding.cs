﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace ScriptableObjects
        {
            /// <summary>
            /// The base <c>ScriptableSelectableBuilding</c> scriptable object class is will act as data of the buildings inside the
            /// blocktrain.
            /// </summary>
            /// <remarks>
            /// <para>This holds the name, product to product, amount to produce and the demand they need to produce.</para>
            /// </remarks>
            [CreateAssetMenu(fileName = "BuildingData", menuName = "Create Scriptable/BlocktrainQuest/Create new Building")]
            public class ScriptableSelectableBuilding : ScriptableObject
            {
                public string BuildingName;
                public string ProductBToProduce;
                public int AmountToProduce;
                public string ProductDemandName;
            }
        }
    }
}