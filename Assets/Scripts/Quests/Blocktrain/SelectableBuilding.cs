﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ACQ.Blocktrain.ScriptableObjects;
using ACQ.Blocktrain.ChainManagement;
using ACQ.QuestSystem;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace Buildings
        {
            /// <summary>
            /// The <c>SelectableBuilding</c> class handles all the data and functions a building in the blocktrain will have.
            /// </summary>
            /// <remarks>
            /// <para>This scripts needs to be put onto buildings you want to select inside the blocktrain minigame.</para>
            /// </remarks>
            public class SelectableBuilding : MonoBehaviour
            {
                [SerializeField] ScriptableSelectableBuilding m_buildingdata;
                [SerializeField] BuildingInfoShower m_infoshower;
                [SerializeField] ContractMaker m_contractmaker;
                [SerializeField] ChainManager m_chainmanger;
                [SerializeField] bool m_maxproductionatstart;

                [SerializeField] GameObject m_pendingimage;
                [SerializeField] GameObject m_approvedimage;
                [SerializeField] GameObject m_declinedimage;
                [SerializeField] GameObject m_station;

                int m_currentlyproducedgoods;

                void OnEnable()
                {
                    if (m_maxproductionatstart == true)
                    {
                        ProduceToMaxCapacity();
                    }
                }

                /// <summary>
                /// Checks if the buying contract is an acceptable offer.
                /// </summary>
                /// <remarks>
                /// <para>This will simulate a real process that waits a bit between every action that has been taken.</para>
                /// <para>While waiting for the contract an icon will appear that says pending. When the goods have been checked it will either
                /// accept or decline the contract based upon their demands.</para>
                /// </remarks>
                /// <param name="_goods">The goods that will be sent to the buyer.</param>
                public IEnumerator CheckBuyersContract(string _goods)
                {
                    bool approval;

                    m_pendingimage.SetActive(true);
                    yield return new WaitForSeconds(2f);

                    m_pendingimage.SetActive(false);
                    if (_goods == BuildingData.ProductDemandName)
                    {
                        m_approvedimage.SetActive(true);
                        m_chainmanger.AddChain(BuildingData.BuildingName + " accepted the contract");
                        approval = true;
                    }
                    else
                    {
                        m_declinedimage.SetActive(true);
                        m_chainmanger.AddChain(BuildingData.BuildingName + " declined the contract. They don't need " + _goods);
                        approval = false;
                    }

                    yield return new WaitForSeconds(1.1f);
                    m_approvedimage.SetActive(false);
                    m_declinedimage.SetActive(false);
                    m_contractmaker.BuyerAccept(approval);
                }

                /// <summary>
                /// Checks if the sellers contract is an acceptable offer.
                /// </summary>
                /// <remarks>
                /// <para>This will simulate a real process that waits a bit between every action that has been taken.</para>
                /// <para>While waiting for the contract an icon will appear that says pending. When it has checked if the buiding produces
                /// those goods and if they have enough in stock, it will accept or decline the quest based upon that.</para>
                /// </remarks>
                /// <param name="_productamount">The product needed.</param>
                /// <param name="_productname">The amount of goods needed.</param>
                public IEnumerator CheckSellersContract(string _productname, int _productamount)
                {
                    bool approval;

                    m_pendingimage.SetActive(true);
                    yield return new WaitForSeconds(2f);

                    m_pendingimage.SetActive(false);
                    if (_productname == BuildingData.ProductBToProduce)
                    {
                        if (m_currentlyproducedgoods - _productamount > 0)
                        {
                            m_approvedimage.SetActive(true);
                            m_chainmanger.AddChain(BuildingData.BuildingName + " accepted the contract");
                            approval = true;
                        }
                        else
                        {
                            m_declinedimage.SetActive(true);
                            m_chainmanger.AddChain(BuildingData.BuildingName + " declined the contract. They don't have enough " + _productname + " in storage.");
                            approval = false;
                        }
                    }
                    else
                    {
                        m_declinedimage.SetActive(true);
                        m_chainmanger.AddChain(BuildingData.BuildingName + " declined the contract. They don't produce " + _productname + " there.");
                        approval = false;
                    }

                    yield return new WaitForSeconds(1f);
                    m_approvedimage.SetActive(false);
                    m_declinedimage.SetActive(false);
                    m_contractmaker.SellerAccept(approval);
                }

                /// <summary>
                /// Sets the current goods to maximum capacity.
                /// </summary>
                public void ProduceToMaxCapacity()
                {
                    m_currentlyproducedgoods = m_buildingdata.AmountToProduce;
                }

                /// <summary>
                /// Picks up the goods and adds an chain that will indicate that.
                /// </summary>
                public void PickUp()
                {
                    m_currentlyproducedgoods = 0;
                    m_chainmanger.AddChain(BuildingData.BuildingName + " loaded all goods into the train");
                }

                /// <summary>
                /// Delivers the product to the buyer and adds an chain that will indicate that.
                /// </summary>
                /// <remarks>
                /// <para>If the building that the goods will be delivered to is the Mall, it will complete a step towards the <c>Quest</c>.
                /// Because the customer can now have soda and this is the final step it will complete the Quest</para>
                /// </remarks>
                public void Deliver()
                {
                    m_currentlyproducedgoods = m_buildingdata.AmountToProduce;
                    m_chainmanger.AddChain(BuildingData.BuildingName + " recieved all goods that we need to produce");

                    if (m_buildingdata.BuildingName == "The Mall")
                    {
                        Debug.Log(QuestManager.Instance.CurrentStep);
                        QuestManager.Instance.CompleteStep();
                    }
                }

                /// <summary>
                /// Gets the building data.
                /// </summary>
                public ScriptableSelectableBuilding BuildingData
                {
                    get { return m_buildingdata; }
                }

                /// <summary>
                /// Gets the current amount of goods that have been produced
                /// </summary>
                public int CurrentlyProducedGoods
                {
                    get { return m_currentlyproducedgoods; }
                }

                /// <summary>
                /// Gets the building data.
                /// </summary>
                public GameObject Station
                {
                    get { return m_station; }
                }
            }
        }
    }
}
