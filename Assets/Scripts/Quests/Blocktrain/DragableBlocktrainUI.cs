﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using ACQ.DialogSystem;
using ACQ.Blocktrain.Minigame;
using ACQ.DragAndDrop;

namespace ACQ
{
    namespace Blocktrain
    {
        namespace DragAndDrop
        {
            /// <summary>
            /// <c>DragableBlocktrainUI</c> inherits from <c>DragAndDropable</c>. This class manages the drag and dropable UI icons in the blocktrain.
            /// </summary>
            public class DragableBlocktrainUI : DragAndDropable
            {
                [SerializeField] BlocktrainDropableMinigame m_dropablemanager;
                [SerializeField] Camera m_camera;
                [SerializeField] string m_buildingtoplace;

                [SerializeField] Transform m_startpos;

                [SerializeField] Dialog m_wrongplacementdialog;

                /// <summary>
                /// Gets called whenever you're dragging this gameobject. It stays at the same position as where the user touches.
                /// </summary>
                /// <param name="eventData">Data of the current pointer.</param>
                public override void OnDrag(PointerEventData eventData)
                {
                    if (!DialogManager.Instance.InConversation)
                    {
                        gameObject.transform.position = Input.GetTouch(0).position;
                    }
                }

                /// <summary>
                /// Gets called when you release the gameobject.
                /// </summary>
                /// <remarks>
                /// <para>It will cast a ray at touch location and check if the hitted object
                /// is a droppoint. If it is a droppoint it will checks what object it hit and if its the correct plot. Else it will just
                /// reset the position.</para>
                /// </remarks>
                /// <param name="eventData">Data of the current pointer.</param>
                public override void OnEndDrag(PointerEventData eventData)
                {
                    if (!DialogManager.Instance.InConversation)
                    {
                        Ray ray = m_camera.ScreenPointToRay(Input.GetTouch(0).position);
                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("Droppoint"))
                        {
                            if (m_buildingtoplace == hit.collider.gameObject.GetComponent<DropPoint>().BuildToChangeInto)
                            {
                                m_dropablemanager.CheckOnRelease(m_buildingtoplace, hit.collider.gameObject.GetComponent<DropPoint>().BuildToChangeInto);
                            }
                            else
                            {
                                //if the object isn't a a dropable point
                                ResetPosition();
                            }
                        }
                        else
                        {
                            //if you didn't hit anything
                            ResetPosition();
                        }
                    }
                }

                /// <summary>
                /// Resets the position to the same start position it was in.
                /// </summary>
                public void ResetPosition()
                {
                    gameObject.transform.position = m_startpos.position;
                }

                /// <summary>
                /// Gets called right when you touch the screen.
                /// </summary>
                /// <remarks>
                /// <para>Sets the icon tho the one you are holding and attaches to the position of your touch.</para>
                /// </remarks>
                /// <param name="eventData">Data of the current pointer.</param>
                public override void OnPointerDown(PointerEventData eventData)
                {
                    if (!DialogManager.Instance.InConversation)
                    {
                        m_dropablemanager.SetHoldingIcon(this);
                        gameObject.transform.position = Input.GetTouch(0).position;
                    }
                }
            }
        }
    }
}
