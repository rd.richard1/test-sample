﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ACQ
{
    namespace DragAndDrop
    {
        /// <summary>
        /// <c>DragAndDropable</c> is the main class for all drag and dropables.
        /// </summary>
        /// <remarks>
        /// <para>This class holds the OnPointerDown phase, the OnBeginDrag phase, the OnDrag phase and the OnEndDrag phase.</para>
        /// </remarks>
        public class DragAndDropable : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
        {
            public virtual void OnBeginDrag(PointerEventData eventData)
            {

            }

            public virtual void OnDrag(PointerEventData eventData)
            {

            }

            public virtual void OnEndDrag(PointerEventData eventData)
            {

            }

            public virtual void OnPointerDown(PointerEventData eventData)
            {

            }
        }
    }
}
