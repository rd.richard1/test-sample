﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using ACQ.Company;

namespace ACQ
{
    namespace MainMenu
    {
        /// <summary>
        /// The <c>CorrectCodeChecker</c> class manages the code you put in inside the Main Menu.
        /// </summary>
        /// <remarks>
        /// <para>It also changes the Company based upon the code you filled in.</para>
        /// </remarks>
        public class CorrectCodeChecker : MonoBehaviour
        {
            [SerializeField] MainMenuManager m_mainmenumanager;
            [SerializeField] TMP_InputField m_codeinputfield;
            [SerializeField] GameObject m_confirmbutton;
            [SerializeField] Animator m_wrongcodefeedback;

            [SerializeField] CompanyID[] m_companies;

            /// <summary>
            /// Checks if the code that has been put in has more than 4 characters and puts the confirm on active if so.
            /// </summary>
            public void CheckIfCanConfirm()
            {
                if (m_codeinputfield.text.Length == 4)
                {
                    m_confirmbutton.SetActive(true);
                }
                else
                {
                    m_confirmbutton.SetActive(false);
                }
            }

            /// <summary>
            /// Checks the code that has been put in and changes to company reelated to that code.
            /// </summary>
            public void CheckCode()
            {
                switch (m_codeinputfield.text)
                {
                    case "LS_T":
                        PlayingCompany.Instance.CompanyID = m_companies[0];
                        m_mainmenumanager.CompanyThatIsUsingTheApp = PlayingCompany.Instance.CompanyID.CompanyName;
                        m_mainmenumanager.ActivateMainMenu();
                        gameObject.SetActive(false);
                        break;

                    /*
                case "_KLM":
                    PlayingCompany.Instance.CompanyID = m_companies[1];
                    m_mainmenumanager.CompanyThatIsUsingTheApp = PlayingCompany.Instance.CompanyID.CompanyName;
                    m_mainmenumanager.ActivateMainMenu();
                    gameObject.SetActive(false);
                    break;
                    */

                    /*
                case "SHL!":
                    PlayingCompany.Instance.CompanyID = m_companies[2];
                    m_mainmenumanager.CompanyThatIsUsingTheApp = PlayingCompany.Instance.CompanyID.CompanyName;
                    m_mainmenumanager.ActivateMainMenu();
                    gameObject.SetActive(false);
                    break;
                    */

                    default:
                        m_wrongcodefeedback.SetBool("ShowIncorrect", true);
                        m_codeinputfield.text = "";
                        break;
                }
            }
        }
    }
}