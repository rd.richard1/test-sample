﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACQ
{
    namespace MainMenu
    {
        /// <summary>
        /// The <c>IncorrectFeedbackInactiveSetter</c> class manages the animation when you enter the wrong code.
        /// </summary>
        public class IncorrectFeedbackInactiveSetter : MonoBehaviour
        {
            [SerializeField] Animator m_thisanimator;

            public void SwitchToIdle()
            {
                m_thisanimator.SetBool("ShowIncorrect", false);
            }
        }
    }
}