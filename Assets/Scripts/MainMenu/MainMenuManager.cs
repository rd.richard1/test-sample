﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ACQ
{
    namespace MainMenu
    {
        /// <summary>
        /// The <c>MainMenuManager</c> holds everything related to the Main Menu.
        /// </summary>
        public class MainMenuManager : MonoBehaviour
        {
            [SerializeField] GameObject m_mainmenu;
            [SerializeField] GameObject m_codeinputmenu;
            [SerializeField] TextMeshProUGUI m_companywelcometext;

            string m_companythatisusingtheapp;

            /// <summary>
            /// Activates the screen where you can enter the code you got.
            /// </summary>
            public void ActivateCodeInput()
            {
                m_codeinputmenu.SetActive(true);
            }

            /// <summary>
            /// Activates the Main Menu screen and changes the welcome text to fit the playing Company/Player.
            /// </summary>
            public void ActivateMainMenu()
            {
                m_mainmenu.SetActive(true);
                m_companywelcometext.text = "WELCOME" + "\n" + m_companythatisusingtheapp;
            }

            /// <summary>
            /// Loads the AR Enviroment and starts the real tour.
            /// </summary>
            public void StartAR()
            {
                SceneManager.LoadScene("Main Scene");
            }

            /// <summary>
            /// Opens Google Maps and instantly goes to Accenture Liquid Studio, Utrecht.
            /// </summary>
            public void OpenGoogleMaps()
            {
                Application.OpenURL("http://maps.google.com/maps?q=Orteliuslaan 1000, Utrecht");
            }

            /// <summary>
            /// Gets and sets the company of the app.
            /// </summary>
            public string CompanyThatIsUsingTheApp
            {
                get { return m_companythatisusingtheapp; }
                set { m_companythatisusingtheapp = value; }
            }
        }
    }
}