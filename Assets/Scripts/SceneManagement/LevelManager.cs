using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using ACQ.QuestSystem;

namespace ACQ
{
    namespace SceneManagement
    {
        /// <summary>
        /// The <c>LevelManager</c> class contains everything related to ImageTracking.
        /// </summary>
        /// <remarks>
        /// <para>The class also shows or disables objects related to the Image your camera is tracking</para>
        /// </remarks>
        public class LevelManager : MonoBehaviour
        {
            //In the m_scenetoshow you put your gameobjects in that you want to set active
            //MAKE SURE THAT THE OBJECTS ARE AN EQUAL AMOUNT TO THE IMAGES THAT YOU HAVE IN YOUR IMAGE LIBRARY
            //So if the first image in the library has "CITY" as name then the first object in the list should be "CITY" ect.
            [SerializeField] GameObject[] m_scenetoshow;

            ARTrackedImageManager m_artrackedimagemanager;
            Dictionary<string, GameObject> m_dirarobjects;

            void Awake()
            {
                m_artrackedimagemanager = GameObject.FindObjectOfType<ARTrackedImageManager>();
                Debug.Log("Found ArTrackedImageManager...");

                //Sets up the dictionary. Entry 1 is the image name and Entry 2 is the object related to that image.
                m_dirarobjects = new Dictionary<string, GameObject>();
                for (int i = 0; i < m_scenetoshow.Length; i++)
                {
                    m_dirarobjects.Add(m_artrackedimagemanager.referenceLibrary[i].name, m_scenetoshow[i]);
                }
            }

            void OnEnable()
            {
                m_artrackedimagemanager.trackedImagesChanged += OnImageChanged;
            }

            void OnDisable()
            {
                m_artrackedimagemanager.trackedImagesChanged -= OnImageChanged;
            }

            /// <summary>
            /// This gets called whenever the image slightly changes. This is the AR update function
            /// </summary>
            /// <param name="_eventargs">Auto generated parameter by ARFoundation which returns the current search state</param>

            void OnImageChanged(ARTrackedImagesChangedEventArgs _eventargs)
            {
                //_eventargs has 3 different types
                // Added (which is called once), Updated (which is called 60 frames ps) and Removed(which is called when you
                // remove the image you are tracking from your screen.

                foreach (ARTrackedImage _trackedimage in _eventargs.updated)
                {
                    UpdateARImage(_trackedimage);
                }

                foreach (ARTrackedImage _trackedimage in _eventargs.removed)
                {
                    UpdateARImage(_trackedimage);
                }
            }

            /// <summary>
            /// Shows or disables the Image object based upon the QR tracking state.
            /// </summary>
            /// <param name="_trackedimage">The Image that is currently being tracked by the camera.</param>
            void UpdateARImage(ARTrackedImage _trackedimage)
            {
                //If the image is tracked correctly then it will show the object.
                if (_trackedimage.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Tracking)
                {
                    ShowObject(_trackedimage.referenceImage.name, _trackedimage.transform.position, _trackedimage.transform.rotation);
                }

                //If the image is not tracked correctly then it will disable the object.
                else if (_trackedimage.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Limited)
                {
                    DisableObject(_trackedimage.referenceImage.name);
                }
            }

            /// <summary>
            ///  This function takes the name of that you gave in the referenceImage and sets the same name in the directory
            ///  active. It will also put the object on the same position and rotation as the QR.
            /// </summary>
            /// <param name="pos">The position of the Image.</param>
            /// <param name="rot">The rotation of the Image.</param>
            /// <param name="_nameofobjtoshow">Name of the Image.</param>
            void ShowObject(string _nameofobjtoshow, Vector3 pos, Quaternion rot)
            {
                //The City can only be shown without a quest
                if (!QuestManager.Instance.HasQuest && _nameofobjtoshow == "City")
                {
                    m_dirarobjects["City"].SetActive(true);
                    m_dirarobjects["City"].transform.position = pos;
                    m_dirarobjects["City"].transform.rotation = rot;
                }

                if (QuestManager.Instance.HasQuest && _nameofobjtoshow == QuestManager.Instance.CurrentQuest.QRCode)
                {
                    m_dirarobjects[_nameofobjtoshow].SetActive(true);
                    m_dirarobjects[_nameofobjtoshow].transform.position = pos;
                    m_dirarobjects[_nameofobjtoshow].transform.rotation = rot;
                }
            }

            /// <summary>
            /// Disables the object of choice in the Dictionary.
            /// </summary>
            /// <param name="_nameofobject">Name of object to disable.</param>
            void DisableObject(string _nameofobject)
            {
                m_dirarobjects[_nameofobject].SetActive(false);
            }
        }
    }
}