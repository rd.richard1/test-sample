﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACQ
{
    namespace MainMenu
    {
        /// <summary>
        /// The <c>SplashScreenSwitcher</c> class manages the splash screen before the Main Menu.
        /// </summary>
        public class SplashScreenSwitcher : MonoBehaviour
        {
            [SerializeField] GameObject m_menutoswitchto;

            /// <summary>
            /// Switches the screen to the Main Menu.
            /// </summary>
            public void SwitchToMenu()
            {
                m_menutoswitchto.SetActive(true);
                this.gameObject.SetActive(false);
            }
        }
    }
}