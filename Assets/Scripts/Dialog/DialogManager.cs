﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ACQ
{
    namespace DialogSystem
    {
        /// <summary>
        /// The <c>DialogManager</c> is a Singleton class that handles dialog relatd actions.
        /// </summary>
        public class DialogManager : MonoBehaviour
        {
            public static DialogManager Instance;

            [SerializeField] Animator m_dialogboxanim;

            [SerializeField] TextMeshProUGUI m_namebox;
            [SerializeField] Image m_picture;
            [SerializeField] TextMeshProUGUI m_sentencebox;

            Queue<string> m_chnames;
            Queue<Sprite> m_chpictures;
            Queue<string> m_chsentences;

            bool m_inconversation;

            void Awake()
            {
                //Singleton Setup
                if (Instance != null && Instance != this)
                {
                    Destroy(this.gameObject);
                    return;
                }

                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }

            void Start()
            {
                //Initiate Queues
                m_chnames = new Queue<string>();
                m_chpictures = new Queue<Sprite>();
                m_chsentences = new Queue<string>();
            }

            /// <summary>
            /// Sets up the DialogSystem for usage. Also starts the conversation.
            /// </summary>
            /// <example>
            /// <code>
            /// DialogManager.Instance.StartDialog(firstconversation_0);
            /// </code>
            /// </example>
            /// <param name="_dialog">The dialog that will be played</param>
            public void StartDialog(Dialog _dialog)
            {
                m_inconversation = true;

                m_chnames.Clear();
                m_chpictures.Clear();
                m_chsentences.Clear();

                foreach (string name in _dialog.CharacterNames)
                {
                    m_chnames.Enqueue(name);
                }
                foreach (Sprite pic in _dialog.CharacterSprites)
                {
                    m_chpictures.Enqueue(pic);
                }
                foreach (string sentence in _dialog.CharacterSentences)
                {
                    m_chsentences.Enqueue(sentence);
                }

                m_dialogboxanim.SetBool("IsOpen", true);

                DisplayNextDialog();
            }

            /// <summary>
            /// Displays the next piece of dialog.
            /// </summary>
            /// <remarks>
            /// <para>This takes every first entry of each Queue and displays it. It then dequeues the entry, 
            /// so that it can move on to the next entry.</para>
            /// <para>When all the Queues are empty it ends the dialog.</para>
            /// </remarks>
            public void DisplayNextDialog()
            {
                if (m_chsentences.Count == 0 && m_chpictures.Count == 0 && m_chsentences.Count == 0)
                {
                    EndDialog();
                    return;
                }

                string name = m_chnames.Dequeue();
                Sprite pic = m_chpictures.Dequeue();
                string sentence = m_chsentences.Dequeue();

                m_namebox.SetText(name);
                m_picture.sprite = pic;
                m_sentencebox.SetText(sentence);
            }

            /// <summary>
            /// Closes the current <c>Dialog</c> and ends the conversation.
            /// </summary>
            void EndDialog()
            {
                m_dialogboxanim.SetBool("IsOpen", false);
                m_inconversation = false;
            }

            /// <summary>
            /// Gets if you are in a conversation or not.
            /// </summary>
            public bool InConversation
            {
                get { return m_inconversation; }
            }
        }
    }
}
