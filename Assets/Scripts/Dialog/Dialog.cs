﻿using UnityEngine;

namespace ACQ
{
    namespace DialogSystem
    {
        /// <summary>
        /// The base <c>Dialog</c> scriptable object class. This contains all the data of a Dialog.
        /// </summary>
        /// <remarks>
        /// <para>Each list entry will be part of a Queue. So each list needs to be filled in like a conversation.
        /// EXAPLE: Name = Lisa, Sprite = Shopowner, Sentece = "Hello. How are you?"</para>
        /// </remarks>
        [CreateAssetMenu(fileName = "DialogData", menuName = "Create Scriptable/New Dialog", order = 2)]
        public class Dialog : ScriptableObject
        {
            //The data types for the dialog system
            //You can add more if you want to, like sounds for example per dialog.
            public string[] CharacterNames;
            public Sprite[] CharacterSprites;
            [TextArea(3, 10)] public string[] CharacterSentences;
        }
    }
}
