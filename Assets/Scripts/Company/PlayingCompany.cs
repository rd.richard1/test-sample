﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACQ
{
    namespace Company
    {
        /// <summary>
        /// This <c>PlayingCompany</c> class is an Singleton that manages the current selected Company.
        /// </summary>
        public class PlayingCompany : MonoBehaviour
        {
            public static PlayingCompany Instance;

            CompanyID m_company;

            void Awake()
            {
                //Singleton Setup
                if (Instance != null && Instance != this)
                {
                    Destroy(this.gameObject);
                    return;
                }

                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }

            /// <summary>
            /// Get or set the current Company.
            /// </summary>
            /// <returns>The current Company</returns>
            public CompanyID CompanyID
            {
                get { return m_company; }
                set { m_company = value; }
            }
        }
    }
}
