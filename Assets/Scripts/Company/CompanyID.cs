﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACQ
{
    namespace Company
    {
        /// <summary>
        /// The <c>CompanyID</c> class is a scriptable object class which you can give company related data.
        /// </summary>
        [CreateAssetMenu(fileName = "CompanyInfo", menuName = "Create Scriptable/New Company", order = 2)]
        public class CompanyID : ScriptableObject
        {
            //This is part of the lateer to be made Company based experience
            //in which each company would have their own city enviroment dedicated to them.
            //EXAMPLE: KLM would have an Airfield instead of a City.

            public string CompanyName;
            public Texture2D CompanyLogo;
            public int CompanyEmployeeCount;
        }
    }
}
