﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace ACQ
{
    namespace UpgradeSystem
    {
        /// <summary>
        /// The <c>BuildingUpgrader</c> class handles the upgrading process of the city.
        /// </summary>
        ///<remarks>
        ///<para>This class is a Singleton.</para>
        /// </remarks>
        public class BuildingUpgrader : MonoBehaviour
        {
            public static BuildingUpgrader Instance;

            int m_citylevelindicator;

            void Awake()
            {
                //Singleton Setup
                if (Instance != null && Instance != this)
                {
                    Destroy(this.gameObject);
                    return;
                }

                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }

            /// <summary>
            /// Sets a number of objects on active so that they will be shown inside the city.
            /// </summary>
            /// <remarks>Waits a bit after turning a object active.</remarks>
            /// <param name="_buildings">The objects that will be set on active.</param>
            /// <param name="_waittime">The amount of time between each activated object.</param>
            public IEnumerator IUpgradeBuildings(GameObject[] _buildings, float _waittime)
            {
                //Enable the next city level
                for (int b = 0; b < _buildings.Length; b++)
                {
                    _buildings[b].SetActive(true);
                    yield return new WaitForSecondsRealtime(0.1f);
                }

                m_citylevelindicator++;
            }
        }
    }
}